from django.db import models

# Create your models here.
class Category(models.Model):
    """docstring for Category."""
    description = models.CharField( 
                                   max_length = 100, 
                                   help_text = "Description category", 
                                   unique = True )
    
    def __str__(self):
        return '{}'.format( self.description )
      
    class Meta:
        verbose_name_plural = "Categories"