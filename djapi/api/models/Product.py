from django.db import models

from api.models import SubCategory

class Product(models.Model):
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    description = models.CharField(
        max_length=100,
        help_text='Descripción del Producto',
        unique=True
    )
    created_at = models.DateTimeField()
    vendido = models.BooleanField(default=False)
 
    def __str__(self):
        return '{}'.format(self.description)
 
    class Meta:
        verbose_name_plural = "Products"