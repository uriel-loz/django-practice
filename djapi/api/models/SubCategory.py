from django.db import models
from api.models import Category

class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(
        max_length=100,
        help_text='Descripción de la Sub Categoría'
    )
 
    def __str__(self):
        return '{}:{}'.format(self.category.description,self.description)
 
    class Meta:
        verbose_name_plural = "Sub categories"
        unique_together = ('category','description')