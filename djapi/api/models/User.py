from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

class UserManager( BaseUserManager ):
    """ Manager para usuario """
    
    def create_user( self, email, name, last_name, password ):
        """ Crear nuevo usuario """
        if not email:
            raise ValueError( 'User must have an email' )
        
        email = self.normalize_email( email )
        user = self.model( email = email, name = name, last_name = last_name )
        
        user.set_password( password )
        user.role = 'user' 
        user.save( using = self._db )
        
        return user
    
    def create_superuser( self, email, name, last_name, password  ):
        user = self.create_user( email, name, last_name, password )
        
        user.is_superuser = True
        user.role = 'admin'
        user.is_staff = True
        user.save( using = self._db )
        
        return user

class User( AbstractBaseUser, PermissionsMixin ):
    """ Model Base de datos para usuarios en el sistema """
    name = models.CharField( max_length = 60 )
    last_name = models.CharField( max_length = 60 )
    email = models.CharField( max_length = 150, unique = True )
    role = models.CharField( max_length = 20, blank=True )
    password = models.CharField( max_length = 255 )
    is_staff = models.BooleanField( default = False )
    created_at = models.DateTimeField( auto_now = True, auto_now_add = False, blank=True, null=True )
    updated_at = models.DateTimeField( auto_now = False, auto_now_add = True, blank=True, null=True )
    
    objects = UserManager()
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [ 'name', 'last_name', 'password' ]
    
    def getFullName( self ):
        return self.name + ' ' + self.last_name
    
    def __str__( self ):
        return self.email