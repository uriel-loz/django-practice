from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from api.models import  Product, SubCategory, Category, User

class ProductSerializer(serializers.ModelSerializer):
    """docstring for ProductSerializers."""
    class Meta:
        model = Product
        fields = '__all__'
        
class CategorySerializer(serializers.ModelSerializer):
    """docstring for CategorySerializers."""
    class Meta:
        model = Category
        fields = '__all__'
        
class SubCategorySerializer(serializers.ModelSerializer):
    """docstring for SubCategorySerializers."""
    class Meta:
        model = SubCategory
        fields = '__all__'
        
class UserSerializer( serializers.ModelSerializer ):
    class Meta:
        model = User
        fields = ( 'name', 'last_name', 'role', 'email', 'password' )
        # extra_kwargs = { 'password': { 'write_only': True } }
            