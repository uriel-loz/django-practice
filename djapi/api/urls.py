from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import CategoryDetail, CategoryStore, Index, ProductViewSet, Show, SubCategoryAdd, SubCategoryList, SubCategoryStore, UserCreate

router = DefaultRouter()
router.register( 'v2/products', ProductViewSet, 'products' )

urlpatterns = [
    path( "v1/products/", Index.as_view(), name = 'index' ),
    path( "v1/products/<int:pk>", Show.as_view(), name = 'show' ),
    path( "v1/categories/", CategoryStore.as_view(), name = 'categories-store' ),
    path( "v1/subcategories/", SubCategoryStore.as_view(), name = 'subcategories-store' ),    
    path( 'v1/categories/<int:pk>', CategoryDetail.as_view(),name='category-detail' ),
    path( 'v1/categories/<int:pk>/subcategories/', SubCategoryList.as_view(),name='category-list' ),
    path( 'v1/categories/<int:pk>/subcategories-add/', SubCategoryAdd.as_view(),name='category-add' ),
    
    path("v3/users/", UserCreate.as_view(), name="users"),
]

urlpatterns += router.urls