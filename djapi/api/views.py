# from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import viewsets

from api.models import Product, SubCategory, Category, User
from .serializers import CategorySerializer, ProductSerializer, SubCategorySerializer, UserSerializer
# Create your views here.

# class Index( APIView ):
    # def get( self, request ):
    #     products = Product.objects.all()[ :20 ]
    #     data = ProductSerializer( products, many = True ).data
        
    #     output = {
    #         'code': 200,
    #         'status': 'success',
    #         'message': 'Operation Successfull',
    #         'data': data
    #     }
        
    #     return Response( output )

# class Show( APIView ):
#     def get( self, request, pk ):
#         product = get_object_or_404( Product, pk = pk )
#         data = ProductSerializer( product ).data
        
#         output = {
#             'code': 200,
#             'status': 'success',
#             'message': 'Operation Successfull',
#             'data': data
#         }
        
#         return Response( output )

class Index( generics.ListCreateAPIView ):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    
class Show( generics.RetrieveUpdateDestroyAPIView ):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    
class CategoryStore( generics.CreateAPIView ):
    serializer_class = CategorySerializer
    
class SubCategoryStore( generics.CreateAPIView ):
    serializer_class = SubCategorySerializer
    
class CategoryDetail(generics.RetrieveDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    
class SubCategoryList(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = SubCategory.objects.filter(category_id=self.kwargs["pk"]).order_by( 'id' )
        return queryset
    
    serializer_class = SubCategorySerializer
    
class SubCategoryAdd( APIView ):
    def post( self, request, pk ):
        description = request.data.get( 'description' )
        data = {
            'description': description,
            'category': pk
        }
        
        serialize = SubCategorySerializer( data = data )
        
        if serialize.is_valid():
            serialize.save()
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull'
            }
        else :
            output  = {
                'code': 0,
                'status': 'error',
                'message': serialize.errors
            }
            
        return Response( output )
    
class ProductViewSet(viewsets.ModelViewSet):
    """docstring for ProductViewSet."""
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    
    # Custom ViewSet
    # def list(self, request):
    #     pass

    # def create(self, request):
    #     pass

    # def retrieve(self, request, pk=None):
    #     pass

    # def update(self, request, pk=None):
    #     pass

    # def partial_update(self, request, pk=None):
    #     pass

    # def destroy(self, request, pk=None):
    #     pass    

# class UserCreate( generics.ListCreateAPIView ):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer    

class UserCreate(APIView):
    def get( self, request ):
        users = User.objects.all()
        data = UserSerializer( users, many = True ).data
        
        output = {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': data
        }
        
        return Response( output )

    
    def post( self, request ):   
        data = request.data;
                
        serializer = UserSerializer( data = data )
        
        if serializer.is_valid():
            valid_data = serializer.data
            
            User.objects.create_user( **valid_data )
         
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull'
            }
        else :
            output  = {
                'code': 0,
                'status': 'error',
                'message': serializer.errors
            }
            
        return Response( output )
    
