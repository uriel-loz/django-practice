from django.db import models
from django.db.models.fields import CharField

# Create your models here.
class Category(models.Model):
    """docstring for Category."""
    description = models.CharField(max_length = 100 )
    active = models.BooleanField( default = True )
    
    def __str__(self):
        return '{}'.format( self.description )
    
    class Meta:
        verbose_name_plural = "Categories"
    